package com.framework;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bui on 3/31/2015.
 */
public class ViewAndModel {

    private String view;
    private Map<String,Object> model = new HashMap<String,Object>(3);


    public ViewAndModel(String view) {
        this.view = view;
    }

    public final  <T> T getModel(String name){
        return (T)model.get(name);
    }

    public final  ViewAndModel withModel(String name,Object data){
        model.put(name,data);
        return this;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
}
