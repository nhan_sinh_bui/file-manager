package com.examples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.flystar.domain.Document;
import com.flystar.domain.ElasticDocument;
import com.flystar.domain.OpenUser;
import com.flystar.service.OfyService;
import com.flystar.utils.ErrorHandler;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.FileInfo;
import com.google.appengine.api.oauth.OAuthService;
import com.google.appengine.api.oauth.OAuthServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;


public class Upload extends HttpServlet {
    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    private static Queue searchQueue = QueueFactory.getQueue("elastic-search");
    private static Queue driveQueue = QueueFactory.getQueue("google-drive");
    private static final ErrorHandler errorHandler = new ErrorHandler();
    private static Logger log = Logger.getLogger(Upload.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        OAuthService oauth = OAuthServiceFactory.getOAuthService();
        User user = null;
        try{
            user = oauth.getCurrentUser();
        }catch(Exception e){

            resp.getWriter().println(new Gson().toJson(e.toString()));
            return;
        }


        String thisURL = req.getRequestURI();

        resp.setContentType("text/html");
        if (user != null) {
            resp.getWriter().println(new Gson().toJson(user));
        } else {
            resp.getWriter().println("<p>Please <a href=\"" +

                    "\">sign in</a>.</p>");
        }

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        try {
            String userId = UserServiceFactory.getUserService().getCurrentUser().getEmail();
            if(userId == null) {
                errorHandler.postError(400, "invalid username", res);
                return;
            }

            Key<OpenUser> userKey = Key.create(OpenUser.class,userId);
            OpenUser openUser = OfyService.ofy().load().key(userKey).now();
            if(openUser == null){
                openUser = new OpenUser();
                openUser.setEmail(userId);
                OfyService.ofy().save().entity(openUser);
            }

            Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
            Map<String, List<FileInfo>> infos = blobstoreService.getFileInfos(req);
            List<BlobKey> blobKeys = blobs.get("file");


            Document document = new Document();
            document.setCreatedBy(userId);
            document.setTitle(req.getParameter("fileTitle"));
            document.setDescription(req.getParameter("fileDescription"));
            String tags = req.getParameter("fileTags");
            if(tags != null){
                document.setTags(Arrays.asList(tags.split(",")));
            }

            document.setCloudStorageLink(infos.get("file").get(0).getGsObjectName());



            Key<Document> key = OfyService.factory().allocateId(userKey,Document.class);
            document.setId(key.getId());
            document.setOwner(userKey);
            OfyService.ofy().save().entities(openUser,document).now();
            ElasticDocument elasticDocument = new ElasticDocument(document);
            elasticDocument.setOwner(userId);
            elasticDocument.setMimeType(infos.get("file").get(0).getContentType());
            addTasksToQueue(elasticDocument);

            req.setAttribute("uploadFileStatus",Boolean.TRUE);

        }catch(Exception e){
            req.setAttribute("uploadFileStatus",Boolean.FALSE);
            log.severe(e.getMessage());

        }
        req.getRequestDispatcher("/uploadFile.jsp").forward(req,res);

//        res.getWriter().println(new Gson().toJson(new Result(openUser,document).addResult(req.getUserPrincipal().getName())));

    }

    private void addTasksToQueue(ElasticDocument elasticDocument) {
        String json = new Gson().toJson(elasticDocument);
        searchQueue.add(TaskOptions.Builder.withUrl("/tasks/elasticUpdate").param("elasticDocument",json));
        driveQueue.add(TaskOptions.Builder.withUrl("/tasks/googleDriveUpdate").param("elasticDocument",json));

    }
}


class Result{
    private OpenUser openUser;
    private Document document;
    private List<Object> list;


    public Result(OpenUser openUser, Document document) {
        this.openUser = openUser;
        this.document = document;
    }

    public OpenUser getOpenUser() {
        return openUser;
    }

    public void setOpenUser(OpenUser openUser) {
        this.openUser = openUser;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Result addResult(Object object){
        if(list == null) {
            list = new ArrayList(5);
        }
        list.add(object);
        return this;
    }
}