package com.examples;

import com.flystar.utils.ErrorHandler;
import com.flystar.utils.IdGenerator;
import com.flystar.utils.MimeDetector;
import com.google.api.client.util.ByteStreams;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.repackaged.com.google.common.io.Files;
import com.google.appengine.tools.cloudstorage.*;
import com.google.gson.JsonObject;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.jglue.fluentjson.JsonBuilderFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.Random;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FileUpload extends HttpServlet {
    public static final boolean SERVE_USING_BLOBSTORE_API = true;
    private static final int RANDOM_LENGTH = 10;
    private static final Logger log = Logger.getLogger(FileUpload.class.getName());
    private static final String BUCKET_NAME = "presentationfile";

    private static final int BUFFER_SIZE = 2 * 1024 * 1024;




    private static final MimeDetector mimeDetector = new MimeDetector();
    private static final ErrorHandler errorHandler = new ErrorHandler();
    private static final IdGenerator idGenerator = new IdGenerator();

    private final UserService userService = UserServiceFactory.getUserService();
    private final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
            .initialRetryDelayMillis(10)
            .retryMaxAttempts(10)
            .totalRetryPeriodMillis(15000)
            .build());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setHeader("Access-Control-Allow-Origin","*");
        res.setContentType("application/json");
        JsonObject jsonObject = JsonBuilderFactory.buildObject()
                .add("uploadLink",getUploadLink()).getJson();
        res.getWriter().println(jsonObject);
    }

    private String getUploadLink(){
       return blobstoreService.createUploadUrl("/upload", UploadOptions.Builder.withGoogleStorageBucketName("presentationfile"));
    }

  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
      res.setHeader("Access-Control-Allow-Origin","*");
//      if(!authorizeUser(req,res)){
//          return;
//      };
      handleUploadData(req,res);


  }

    private boolean authorizeUser(HttpServletRequest req, HttpServletResponse res) throws IOException{
        res.setContentType("text/html");

        res.getWriter().println("<!doctype><html><body><h2>GAE - Integrating Google user account</h2></body></html>");
        User user = userService.getCurrentUser();
        if (user != null) {

            res.getWriter().println("Welcome, " + user.getNickname());
            res.getWriter().println(
                    "<a href='"
                            + userService.createLogoutURL(req.getRequestURI())
                            + "'> LogOut </a>");
            return true;

        } else {

            res.getWriter().println(
                    "Please <a href='"
                            + userService.createLoginURL(req.getRequestURI())
                            + "'> LogIn </a>");
            return false;

        }
    }

    private void handleUploadData(HttpServletRequest req, HttpServletResponse res) throws ServletException{
        try {
            ServletFileUpload upload = new ServletFileUpload();


            FileItemIterator iterator = upload.getItemIterator(req);
            while (iterator.hasNext()) {
                FileItemStream item = iterator.next();
                InputStream stream = item.openStream();

                if (item.isFormField()) {
                    log.warning("Got a form field: " + item.getFieldName());
                } else {
                    log.warning("Got an uploaded file: " + item.getFieldName() +
                            ", name = " + item.getName());
                    String fileExt = Files.getFileExtension(item.getName());
                    String name = Files.getNameWithoutExtension(item.getName());
                    if(mimeDetector.getMimeType(fileExt) == null){
                        log.warning("Unsupported File Extension");
                        errorHandler.postError(400,"Unsupported File Extension",res);
                        return;
                    }
                    GcsFilename fileName = generateFileName(fileExt);
                    log.warning("----> file name: "+fileName.getObjectName());
                    GcsOutputChannel outputChannel =
                            gcsService.createOrReplace(fileName, getGcsFileOptions(fileExt));
                    copy(item.openStream(), Channels.newOutputStream(outputChannel));
                }

            }

        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }


    private GcsFilename generateFileName(String fileExt) {

        return new GcsFilename(BUCKET_NAME,idGenerator.generateUniqueFileName(fileExt));

    }




    private void copy(InputStream input, OutputStream output) throws IOException {
        try {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = input.read(buffer);
            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);
            }
        } finally {
            input.close();
            output.close();
        }
    }


    public GcsFileOptions getGcsFileOptions(String ext) {
        return new GcsFileOptions.Builder().mimeType(mimeDetector.getMimeType(ext)).build();
    }
}