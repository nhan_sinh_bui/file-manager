package com.examples;

import com.flystar.domain.ViewLink;
import com.flystar.service.OfyService;
import com.googlecode.objectify.Key;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by bui on 4/3/2015.
 */
public class FileViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
       String gcsLink = req.getParameter("id");

       if(gcsLink==null || gcsLink.trim().length() < 1){
           res.setStatus(400);
           return;
       }
        gcsLink = gcsLink.trim();
        Key<ViewLink> key = Key.create(ViewLink.class,gcsLink);
        ViewLink link = OfyService.ofy().load().key(key).now();
        if(link == null){
            res.getWriter().println("<h3>The file is not ready to be viewed yet! Please try 30 seconds later.</h3><h4>The page will refresh every 15 seconds</h4>");
            res.setIntHeader("Refresh",15);
            return;
        }
        res.sendRedirect(link.getAlternateLink());





    }
}
