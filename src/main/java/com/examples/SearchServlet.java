package com.examples;

import com.flystar.domain.ElasticSearchQuery;
import com.flystar.domain.SearchResult;
import com.framework.ViewAndModel;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

import com.google.gson.*;
import org.jglue.fluentjson.JsonBuilderFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by bui on 3/31/2015.
 */
public class SearchServlet extends HttpServlet {

    private static final String ELASTIC_SERVER_URL = "";
    private static final int PAGE_SIZE = 20;
    private static final URLFetchService fetchService = URLFetchServiceFactory.getURLFetchService();
    private static final Logger log = Logger.getLogger(SearchServlet.class.getName());
    private final ElasticSearchQuery query = new ElasticSearchQuery();

    @Override
    public void init() throws ServletException {
        query.addField("title")
                .addField("description")
                .addField("fileTags")
                .addField("content")
                .addField("owner");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String keyword = req.getParameter("keyword");

        String currentPage = req.getParameter("currentPage");
        ViewAndModel vm = processRequest(keyword,currentPage);
//        ViewAndModel vm = testView(req);
        req.setAttribute("model",vm);
        req.getRequestDispatcher(vm.getView()).forward(req, res);
    }

    private ViewAndModel processRequest(String keyword,String currentPage){
        ViewAndModel vm = new ViewAndModel("/home.jsp");
        int page = 1;
        int from = 0;
        if(keyword == null || keyword.trim().length() < 1){
            return vm;
        }

        keyword = keyword.trim();
        try{
            page = Integer.parseInt(currentPage) - 1;
            if(page > 0) from = page*PAGE_SIZE;
        }catch(Exception e){
            log.warning(e.getMessage());

        }
        try{
            String payload = getSearchQuery(from,PAGE_SIZE,keyword);
            log.info(payload);
            HTTPRequest request = new HTTPRequest(new URL(ELASTIC_SERVER_URL), HTTPMethod.POST);
           request.setPayload(payload.getBytes());
            String result = new String(fetchService.fetch(request).getContent());
            parseResults(result,vm);
            vm.withModel("keyword",keyword);
            vm.withModel("currentPage",page+1);

        }catch(Exception ex){
            ex.printStackTrace();
            log.severe(ex.getMessage());
        }

        return vm;

    }

    private void parseResults(String result, ViewAndModel vm) {
        JsonParser parser = new JsonParser();

        JsonObject object = parser.parse(result).getAsJsonObject().getAsJsonObject("hits");
        int totalResults = object.get("total").getAsInt();
        int totalPages = totalResults/PAGE_SIZE;
        if(totalResults % PAGE_SIZE != 0) ++ totalPages;
        List<SearchResult> searchResults = new ArrayList<SearchResult>(21);
        vm.withModel("totalResults",totalResults)
            .withModel("totalPages",totalPages);

        JsonArray results = object.get("hits").getAsJsonArray();
        for(JsonElement je : results){
            JsonObject jo = je.getAsJsonObject().getAsJsonObject("fields");
            SearchResult sr = new SearchResult();
            sr.setOwner(jo.get("owner").getAsString());
            sr.setTitle(jo.get("title").getAsString());
            sr.setGcsLink(jo.get("gcsLink").getAsString());
            sr.setDescription(jo.get("description").getAsString());
            searchResults.add(sr);
        }
        vm.withModel("result",searchResults);
    }

    private ViewAndModel testView(HttpServletRequest req){
        ViewAndModel vm = new ViewAndModel("/home.jsp");
        List<SearchResult> results = new ArrayList<SearchResult>();
        for(int i = 0; i < 10; ++i){
            SearchResult sr = new SearchResult();
            sr.setTitle("document "+i);
            sr.setOwner("test@example.com");
            sr.setDescription("test test test");
            results.add(sr);
        }
        int currentPage = 1;
        int totalPage = 10;
        int totalResults = 200;

        return  vm.withModel("currentPage",currentPage)
                    .withModel("totalPages", totalPage)
                    .withModel("totalResults",totalResults)
                    .withModel("keyword", req.getParameter("keyword"))
                    .withModel("result", results);
                    

    }

    private String getSearchQuery(int from, int pageSize, String keyword){
        JsonObject object = JsonBuilderFactory.buildObject()
                .addArray("fields")
                    .add("title")
                    .add("description")
                    .add("gcsLink")
                    .add("owner")
                .end()
                .add("from",from)
                .add("size",pageSize)
                .addObject("query")
                    .addObject("bool")
                        .addArray("should")
                            .addObject()
                                .addObject("multi_match")
                                    .add("query",keyword)
                                    .addArray("fields")
                                        .add("title^3")
                                        .add("fileTags^2")
                                        .add("description")
                                        .add("owner")
                                    .end()
                                    .add("minimum_should_match","40%")
                                    .add("type","best_fields")
                                    .add("tie_breaker","0.2")
                                .end()
                            .end()
                            .addObject()
                                .addObject("match_phrase")
                                    .addObject("content")
                                        .add("query",keyword)
                                        .add("slop",42)
                                    .end()
                                .end()
                            .end()
                        .end()
                    .end()
                .end()
                .addArray("sort")
                    .addObject()
                        .addObject("_score")
                            .add("order","desc")
                        .end()
                    .end()
                    .addObject()
                         .addObject("title")
                            .add("order","asc")
                         .end()
                    .end()
                .end()
                .getJson();
        return object.toString();
    }




}
