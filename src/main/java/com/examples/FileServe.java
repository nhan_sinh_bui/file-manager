package com.examples;

import com.flystar.domain.Constants;
import com.flystar.utils.ErrorHandler;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.tools.cloudstorage.*;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.Random;
import java.util.logging.Logger;

public class FileServe extends HttpServlet {
    public static final boolean SERVE_USING_BLOBSTORE_API = true;
    private static final Logger log = Logger.getLogger(FileServe.class.getName());

    private final ErrorHandler errorHandler = new ErrorHandler();

    private static final int BUFFER_SIZE = 2 * 1024 * 1024;

    private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
            .initialRetryDelayMillis(10)
            .retryMaxAttempts(10)
            .totalRetryPeriodMillis(15000)
            .build());

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        log.warning("---> Serving file: "+req.getParameter("blob-key"));
        String key = req.getParameter("blob-key");
        if(key == null || key.trim().length() < 1){
            errorHandler.postError(400, "Invalid file key", res);
            return;
        }

        GcsFilename fileName = new GcsFilename(Constants.GCS_BUCKET,key.trim());
        if (SERVE_USING_BLOBSTORE_API) {
            log.warning("using blob api");
            BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
            BlobKey blobKey = blobstoreService.createGsBlobKey(key);
            blobstoreService.serve(blobKey, res);
        } else {
            log.warning("using normal api");
            res.setContentType(getServletContext().getMimeType(fileName.getObjectName()));

            res.setHeader("Content-disposition","attachment; filename="+fileName.getObjectName());
            GcsInputChannel readChannel = gcsService.openPrefetchingReadChannel(fileName, 0, BUFFER_SIZE);
            copy(Channels.newInputStream(readChannel), res.getOutputStream());
        }
    }



    private void copy(InputStream input, OutputStream output) throws IOException {
        try {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = input.read(buffer);
            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);
            }
        } finally {
            input.close();
            output.close();
        }
    }


}