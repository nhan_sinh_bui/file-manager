package com.flystar.utils;

import java.util.Random;
import java.util.UUID;

/**
 * Created by bui on 3/5/2015.
 */
public class IdGenerator {
    private final Random random = new Random();
    private final char[] characters = {'a','z','h','y','k','d','e','f'};

    public String getRandomString(int length){
        char[] chars = new char[length+1];
        chars[0] = '-';
        for(int i = 1; i <= length;++i){
            chars[i] = characters[random.nextInt(characters.length)];
        }
        return new String(chars);
    }

    public String generateUUID(){
        return UUID.randomUUID().toString();
    }

    public String generateUniqueFileName(String fileExt){
        return String.format("%s.%s",generateUUID(),fileExt);
    }
}
