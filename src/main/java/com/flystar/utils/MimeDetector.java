package com.flystar.utils;

import com.google.appengine.repackaged.com.google.common.io.Files;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bui on 3/5/2015.
 */
public class MimeDetector {

    private static Map<String,String> mimeMap = new HashMap<>();
    private static String DEFAULT = "binary/octet-stream";

    static{
        mimeMap.put("pdf", "application/pdf");
        mimeMap.put("ppt", "application/vnd.ms-powerpoint");
        mimeMap.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");

    }

    public String getMimeType(String ext){
        return mimeMap.get(ext);


    }
}
