package com.flystar.utils;

import com.google.api.client.http.HttpResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.logging.Logger;

/**
 * Created by bui on 3/5/2015.
 */
public class ErrorHandler {
    private static final Logger log = Logger.getLogger(ErrorHandler.class.getName());

    public void postError(int statusCode,String message,HttpServletResponse res){

        try{
            PrintWriter writer = null;
            try{
                writer = new PrintWriter(res.getOutputStream());
                res.setStatus(statusCode);
                writer.println(String.format("<h3>Error: %s</h3>",message));
                writer.close();
            }catch(Exception ex){
                log.warning(ex.getMessage());
            }
            finally {
                if(writer != null) writer.close();
            }
        }catch (Exception ex){
            log.warning(ex.getMessage());
        }


    }
}
