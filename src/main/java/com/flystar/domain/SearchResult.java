package com.flystar.domain;

/**
 * Created by bui on 3/31/2015.
 */
public class SearchResult {
    private String title;
    private String owner;
    private String gcsLink;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getGcsLink() {
        return gcsLink==null?"":gcsLink;
    }

    public void setGcsLink(String gcsLink) {
        this.gcsLink = gcsLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
