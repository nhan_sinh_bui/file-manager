package com.flystar.domain;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.jglue.fluentjson.JsonBuilderFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by bui on 3/31/2015.
 */
public class ElasticSearchQuery {

    private Set<String> fields = new HashSet<>(4);
    private String keyword;

    public ElasticSearchQuery addField(String field){
        fields.add(field);
        return this;
    }

    public ElasticSearchQuery setKeyword(String keyword){
        this.keyword = keyword;
        return this;
    }

    public String getDSLQuery(){
        Gson gson = new Gson();
        JsonObject json = JsonBuilderFactory.buildObject()
                            .addObject("query")
                            .addObject("multi_match")
                            .add("query",keyword)
                            .add("fields",gson.toJson(fields))
                            .end()
                            .end()
                            .getJson();
        return json.toString();
    }

    public String getDSLQuery(String keyword){
        Gson gson = new Gson();
        JsonObject json = JsonBuilderFactory.buildObject()
                .addObject("query")
                .addObject("multi_match")
                .add("query",keyword)
                .add("fields",gson.toJson(fields))
                .end()
                .end()
                .getJson();
        return json.toString();
    }

}
