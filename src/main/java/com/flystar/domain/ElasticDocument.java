package com.flystar.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by bui on 3/31/2015.
 */
public class ElasticDocument {

    private String title;
    private byte[] content; //base64 encoding
    private String description;
    private List<String> fileTags;
    private Date createdDate;
    private String gcsLink;
    private String owner;
    private String mimeType;



    public ElasticDocument() {
    }

    public ElasticDocument(Document document) {
        this.title = document.getTitle();
        this.description = document.getDescription();
        this.fileTags = document.getTags();
        this.createdDate = new Date();
        this.gcsLink = document.getCloudStorageLink();
    }


    public String getGcsLink() {
        return gcsLink;
    }

    public void setGcsLink(String gcsLink) {
        this.gcsLink = gcsLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getFileTags() {
        return fileTags;
    }

    public void setFileTags(List<String> fileTags) {
        this.fileTags = fileTags;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
