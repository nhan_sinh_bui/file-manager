package com.flystar.domain;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by bui on 4/3/2015.
 */

@Entity
@Index
public class ViewLink {
    @Id
    private String gcsLink;
    private String alternateLink;
    private String owner;

    public String getGcsLink() {
        return gcsLink;
    }

    public void setGcsLink(String gcsLink) {
        this.gcsLink = gcsLink;
    }

    public String getAlternateLink() {
        return alternateLink;
    }

    public void setAlternateLink(String alternateLink) {
        this.alternateLink = alternateLink;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
