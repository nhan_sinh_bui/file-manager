package com.flystar.domain;

import com.google.api.server.spi.config.AnnotationBoolean;
import com.google.api.server.spi.config.ApiResourceProperty;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.util.List;

/**
 * Created by bui on 3/6/2015.
 */

@Entity
@Index
public class Document {

    @Id
    private Long id;

    private String cloudStorageLink = "";
    private String createdBy;


    private String title;
    private String description;
    private List<String> tags;


    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Parent
    @ApiResourceProperty(ignored = AnnotationBoolean.TRUE)
    private Key<OpenUser> owner;

    public Key<OpenUser> getOwner() {
        return owner;
    }

    public void setOwner(Key<OpenUser> owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public String getCloudStorageLink() {
        return cloudStorageLink;
    }

    public void setCloudStorageLink(String cloudStorageLink) {
        this.cloudStorageLink = cloudStorageLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}


