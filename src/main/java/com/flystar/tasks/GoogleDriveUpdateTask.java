package com.flystar.tasks;

import com.flystar.domain.Constants;
import com.flystar.domain.DriveDocument;
import com.flystar.domain.ElasticDocument;
import com.flystar.domain.ViewLink;
import com.flystar.service.OfyService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.json.jackson2.JacksonFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;


import com.google.api.services.drive.model.File;
import com.google.appengine.tools.cloudstorage.*;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * Created by bui on 4/2/2015.
 */
public class GoogleDriveUpdateTask extends HttpServlet {

    private static final String GOOGLE_DRIVE_SCOPE = "https://www.googleapis.com/auth/drive";
    private static final String SERVICE_ACCOUNT_USER = "buinhansinh@shinestars.net";
    private static final String SERVICE_ACCOUNT_ID = "107683587367-k7bc0ofj80lhhugjdrrm5v1ip4c29pth@developer.gserviceaccount.com";
    private static final Logger log =  Logger.getLogger(GoogleDriveUpdateTask.class.getName());
    private static final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
    private static Drive driveService;
    private String fullPath;
    private Permission readonlyPermission;
    @Override
    public void init() throws ServletException {
        try{
            readonlyPermission = new Permission();
            readonlyPermission.setRole("reader");
            readonlyPermission.setType("anyone");
            fullPath = getServletContext().getRealPath("/WEB-INF/service-account-key.p12");
            driveService = new Drive.Builder(GoogleNetHttpTransport.newTrustedTransport(),JacksonFactory.getDefaultInstance(),getCredential()).build();


        }catch(Exception e){

            log.severe(e.toString());
            throw new ServletException(e);
        }




    }

    private Credential getCredential() throws Exception{
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(GoogleNetHttpTransport.newTrustedTransport())
                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setServiceAccountId(SERVICE_ACCOUNT_ID)
                .setServiceAccountPrivateKeyFromP12File(new java.io.File(fullPath))


                .setServiceAccountScopes(Collections.singleton(GOOGLE_DRIVE_SCOPE))
                .setServiceAccountUser(SERVICE_ACCOUNT_USER)
                .build();

//        GoogleCredential credential = new GoogleCredential.Builder()
//                .setRequestInitializer(new AppIdentityCredential(Collections.singleton(GOOGLE_DRIVE_SCOPE)))
//                .setServiceAccountUser(SERVICE_ACCOUNT_USER)
//
//                .setTransport(GoogleNetHttpTransport.newTrustedTransport())
//                .setJsonFactory(JacksonFactory.getDefaultInstance())
//                .build();
        return credential;
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String content = req.getParameter("elasticDocument");
        if(content == null){
            log.warning("invalid arguments");
            return;
        }
        try{
            log.info("elasticDocument: "+content);
            ElasticDocument document = new Gson().fromJson(content, ElasticDocument.class);
            String key = document.getGcsLink().split("/")[3];

            GcsFilename filename = new GcsFilename(Constants.GCS_BUCKET,key);

            int fileSize = (int) gcsService.getMetadata(filename).getLength();
            log.warning("file size: " + fileSize);
            ByteBuffer result = ByteBuffer.allocate(fileSize);
            try (GcsInputChannel readChannel = gcsService.openReadChannel(filename, 0)) {
                readChannel.read(result);
            }
            ByteArrayContent bc = new ByteArrayContent(document.getMimeType(),result.array());
            sendUploadRequest(document,bc);
        }catch(Exception e){

            log.warning(e.toString());
            throw new ServletException(e);
        }



    }

    private void sendUploadRequest(ElasticDocument document,ByteArrayContent content) throws Exception{
        File metadata = new File();

        metadata.setTitle(document.getTitle());
        metadata.setDescription(document.getDescription());
        metadata.setLabels(new File.Labels().setRestricted(true));
        metadata.setMimeType(document.getMimeType());
        metadata.setPermissions(getPermissions(document.getOwner()));

        File file = driveService.files().insert(metadata,content).execute();
        driveService.permissions().insert(file.getId(),readonlyPermission).execute();
        saveViewLink(file,document);
    }

    private void saveViewLink(File file, ElasticDocument document) {
        Key<ViewLink> key = Key.create(ViewLink.class,document.getGcsLink());
        ViewLink link = new ViewLink();
        link.setGcsLink(document.getGcsLink());
        link.setAlternateLink(file.getAlternateLink());
        OfyService.ofy().save().entity(link);
        log.info("viewLink Saved");
    }


    private List<Permission> getPermissions(String user){
        List<Permission> permissions = new ArrayList<>(2);
        permissions.add(readonlyPermission);
        return permissions;
    }
}
