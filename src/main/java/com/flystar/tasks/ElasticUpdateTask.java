package com.flystar.tasks;

import com.flystar.domain.Constants;
import com.flystar.domain.ElasticDocument;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.server.spi.config.ApiMethod;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import com.google.appengine.api.urlfetch.*;
import com.google.appengine.repackaged.org.codehaus.jackson.map.ObjectMapper;
import com.google.appengine.tools.cloudstorage.*;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

/**
 * Created by bui on 3/31/2015.
 */
public class ElasticUpdateTask extends HttpServlet {

    private static final BlobstoreService bs = BlobstoreServiceFactory.getBlobstoreService();
    private static final String ELASTIC_SERVER_URL = "";
    private static final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
    private static final URLFetchService urlFetch = URLFetchServiceFactory.getURLFetchService();
    private static final Logger log = Logger.getLogger(ElasticUpdateTask.class.getName());
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

       String content = req.getParameter("elasticDocument");
        if(content == null){
            log.warning("invalid arguments");
            return;
        }
        try{
            log.info("elasticDocument: "+content);
            ElasticDocument document = new Gson().fromJson(content, ElasticDocument.class);
            String key = document.getGcsLink().split("/")[3];

            GcsFilename filename = new GcsFilename(Constants.GCS_BUCKET,key);

            int fileSize = (int) gcsService.getMetadata(filename).getLength();
            log.warning("file size: " + fileSize);
            ByteBuffer result = ByteBuffer.allocate(fileSize);
            try (GcsInputChannel readChannel = gcsService.openReadChannel(filename, 0)) {
                readChannel.read(result);
            }



            document.setContent(result.array());
            sendCreateRequest(document);
        }catch(Exception e){
            log.warning(e.getMessage());
        }



    }

    private void sendCreateRequest(ElasticDocument document) throws Exception {
        log.info("sending http request");
        String payload =  new ObjectMapper().writeValueAsString(document);
        HTTPRequest request = new HTTPRequest(new URL(ELASTIC_SERVER_URL), HTTPMethod.POST);
//        log.info(payload);
        request.setPayload(payload.getBytes());
        log.info(new String(urlFetch.fetch(request).getContent()));
    }
}
