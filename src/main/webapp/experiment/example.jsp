<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/4/2015
  Time: 1:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@ page import="com.google.appengine.api.blobstore.UploadOptions" %>
<%@ page import="com.google.appengine.api.blobstore.UploadOptions.Builder" %>

<%
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    UploadOptions options = Builder.withGoogleStorageBucketName("presentationfile");
%>


<html>
<head>
    <title>Upload Test</title>
</head>
<body>
<form action="<%= blobstoreService.createUploadUrl("/upload",options) %>" method="post" enctype="multipart/form-data">
    <input type="text" name="foo">
    <input type="file" name="myFile">
    <input type="submit" value="Submit">
</form>
</body>
</html>