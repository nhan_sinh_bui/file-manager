'use strict';

/**
 * @ngdoc object
 * @name conferenceApp
 * @requires $routeProvider
 * @requires conferenceControllers
 * @requires ui.bootstrap
 *
 * @description
 * Root app, which routes and specifies the partial html and controller depending on the url requested.
 *
 */
var app = angular.module('conferenceApp',
    ['conferenceControllers','ui.bootstrap','ui.router',"LocalStorageModule"]).
    config(['$stateProvider','$urlRouterProvider','$httpProvider',
        function ($stateProvider,$urlRouterProvider,$httpProvider) {

            //$stateProvider.
            //    state('apiExample', {
            //        url:"/example",
            //        templateUrl: 'api_examples/index.html'
            //        //controller: 'ShowConferenceCtrl'
            //    }).
            //    state('customerDashboard', {
            //        url:"/customer",
            //        templateUrl: 'view_customer/customer_dashboard.html',
            //        controller: 'ShowConferenceCtrl'
            //    }).
            //    state('showFiles', {
            //        parent:"customerDashboard",
            //        url:"/showFiles",
            //        templateUrl: 'view_customer/show_files.html',
            //        controller: 'ShowFileCtrl'
            //    }).
            //    state('createFiles', {
            //        parent:"customerDashboard",
            //        url:"/create",
            //        templateUrl: 'view_forms/create_file.html',
            //        controller: 'FileCreateCtrl'
            //    }).
            //    state('viewFiles', {
            //        parent:"customerDashboard",
            //        url:"//detail/:websafeConferenceKey",
            //        templateUrl: 'view_customer/conference_detail.html',
            //        controller: 'ConferenceDetailCtrl'
            //    }).
            //
            //    state('profiles', {
            //        parent:"customerDashboard",
            //        url:"//detail/:websafeConferenceKey",
            //        templateUrl: 'view_customer/profile.html',
            //        controller: 'MyProfileCtrl'
            //    }).
            //    state('home', {
            //        url:"/home",
            //        templateUrl: 'view_home/home.html'
            //    })
            //$urlRouterProvider.otherwise("/example")

        }]);

/**
 * @ngdoc filter
 * @name startFrom
 *
 * @description
 * A filter that extracts an array from the specific index.
 *
 */
app.filter('startFrom', function () {
    /**
     * Extracts an array from the specific index.
     *
     * @param {Array} data
     * @param {Integer} start
     * @returns {Array|*}
     */
    var filter = function (data, start) {
        return data.slice(start);
    }
    return filter;
});


/**
 * @ngdoc constant
 * @name HTTP_ERRORS
 *
 * @description
 * Holds the constants that represent HTTP error codes.
 *
 */
app.constant('HTTP_ERRORS', {
    'UNAUTHORIZED': 401
});


/**
 * @ngdoc service
 * @name oauth2Provider
 *
 * @description
 * Service that holds the OAuth2 information shared across all the pages.
 *
 */
app.factory('oauth2Provider', function ($modal,localStorageService) {
    var oauth2Provider = {
        CLIENT_ID: '749222696210-1c973p2djlh720n6lmn1kc4qjsa9g154.apps.googleusercontent.com',
        SCOPES: 'https://www.googleapis.com/auth/userinfo.email profile',
        signedIn: false

    };
    //if(localStorageService.get("accessToken")){
    //    alert(localStorageService.get("accessToken"));
    //    oauth2Provider.signedIn = true;
    //    gapi.auth.setToken({"access_token":localStorageService.get("accessToken")});
    //}
    //alert("sign in "+oauth2Provider.signedIn);
    /**
     * Calls the OAuth2 authentication method.
     */
    oauth2Provider.signIn = function (callback) {
        gapi.auth.signIn({
            'clientid': oauth2Provider.CLIENT_ID,
            'cookiepolicy': 'single_host_origin',
            'accesstype': 'online',
            'approveprompt': 'auto',
            'scope': oauth2Provider.SCOPES,
            'callback': function(){

                callback();
            }

        });
    };


    oauth2Provider.getUsername = function(callback){
        if(oauth2Provider.signedIn){
            var username = null;
            if(gapi.auth.getToken() != null ){
                localStorageService.get(gapi.auth.getToken().access_token);
            }
            if(username == null){
                gapi.client.oauth2.userinfo.get().execute(function (resp) {
                    if (resp.email) {
                        oauth2Provider.signedIn = true;
                        localStorageService.set(gapi.auth.getToken().access_token,resp.email);
                        username=resp.email;
                    }
                    callback(username);


                    });


            }else{
                callback(username);
            }
        }
        else{
            callback(null);
        }

    }

    /**
     * Logs out the user.
     */
    oauth2Provider.signOut = function () {

        gapi.auth.signOut();
        alert("after sign out");
        localStorageService.remove("email");
        gapi.auth.setToken(null);


        // Explicitly set the invalid access token in order to make the API calls fail.

        oauth2Provider.signedIn = false;
    };

    /**
     * Shows the modal with Google+ sign in button.
     *
     * @returns {*|Window}
     */
    oauth2Provider.showLoginModal = function() {
        var modalInstance = $modal.open({
            templateUrl: '/partials/login.modal.html',
            controller: 'OAuth2LoginModalCtrl'
        });
        return modalInstance;
    };

    return oauth2Provider;
});

