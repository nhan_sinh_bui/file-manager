<%@ page import="com.flystar.domain.Document" %>
<%@ page import="java.util.List" %>
<%@ page import="com.flystar.domain.OpenUser" %>
<%@ page import="com.googlecode.objectify.Key" %>
<%@ page import="com.flystar.service.OfyService" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="java.net.URLEncoder" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 10:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%
    request.setAttribute("activeLink","profile");
    response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My File</title>
    <%@include file="secure.jsp"%>
    <%@include file="header.jsp"%>
</head>
<body>
<header>
    <%@include file="navigation.jsp"%>
</header>
<section style="margin-top:80px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h2>Files</h2>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:30%">Title</th>


                </tr>
                </thead>
                <tbody>

                    <%
                        User user =  UserServiceFactory.getUserService().getCurrentUser();
                        if(user == null) return;
                        String userId =user.getEmail();
                        Key<OpenUser> userKey = Key.create(OpenUser.class, userId);
                        List<Document> list = OfyService.ofy().load().type(Document.class)
                                .ancestor(userKey)

                                .list();

                        for(Document dc : list){
                            out.println("<tr>");
                            out.println(String.format("<td>%s<td>",dc.getTitle()));
                            out.println(String.format("<td>%s<td>",dc.getDescription()));
                            out.println(String.format("<td><a href='%s' target='_blank'>%s</a><td>","/fileView?id="+ URLEncoder.encode(dc.getCloudStorageLink(), "UTF-8"),"View"));
                            out.println(String.format("<td><a href='%s' target='_blank'>%s</a><td>", "/fileServe?blob-key=" + URLEncoder.encode(dc.getCloudStorageLink(),"UTF-8"),"Download"));
                            out.println("</tr>");
                        }
                    %>


                </tbody>
            </table>
        </div>
    </div>
</section>

</body>
</html>
