<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.UploadOptions" %>
<%@ page import="com.flystar.domain.Constants" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 10:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%
    request.setAttribute("activeLink","newFile");
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload File</title>
    <%@include file="secure.jsp"%>
    <%@include file="header.jsp"%>
</head>
<body>
<header>
    <%@include file="navigation.jsp"%>
</header>
<section style="margin-top:80px;">
    <div class="container">
        <div class="row" >
            <div class="col-md-8">
                <form
                        action="<%=blobstoreService.createUploadUrl("/upload", UploadOptions.Builder.withGoogleStorageBucketName(Constants.GCS_BUCKET))%>"
                        method="post"
                        enctype="multipart/form-data"

                      class="form-horizontal" role="form">
                    <fieldset>

                        <!-- Form Name -->


                        <!-- Text input-->
                        <%

                            Boolean status = (Boolean)request.getAttribute("uploadFileStatus");
                            String message = "";
                            if(status == Boolean.TRUE){
                                message = "New File added successfully";
                            }
                            else if(status == Boolean.FALSE){
                                message = "Error when processing file!";
                            }
                        %>

                        <div class="form-group">
                            <label class="col-sm-12 control-label" ><%=message%></label>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >Title*</label>
                            <div class="col-sm-8">
                                <input name="fileTitle" type="text" placeholder="File Title" class="form-control" required>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >Description</label>
                            <div class="col-sm-8">
                                <input name="fileDescription" type="text" placeholder="Description" class="form-control">
                            </div>
                        </div>


                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-sm-4 control-label" >Tags*</label>
                            <div class="col-sm-8">
                                <input  type="text" placeholder="Tags" name="fileTags" class="form-control" required>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-sm-4 control-label"> Choose File</label>
                            <div class="col-sm-8">
                                <input type="file" name="file" required>

                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="pull-right">
                                    <input class="btn btn-default" type="submit"  value="Upload File"/>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
    </div>

</section>

</body>
</html>
