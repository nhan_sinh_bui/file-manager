<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.net.URLEncoder" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 10:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%
    String redirectUri = request.getRequestURI();
    if(UserServiceFactory.getUserService().getCurrentUser() == null){
        response.sendRedirect(String.format("/auth.jsp?redirectUri=%s", URLEncoder.encode(redirectUri,"UTF-8")));
    }
%>
