<%@ page import="com.framework.ViewAndModel" %>
<%@ page import="com.flystar.domain.SearchResult" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 10:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setAttribute("activeLink","homepage");
%>
<html>
<head>
    <title>My File</title>

    <%@include file="header.jsp"%>
</head>
<body>
<header>
    <%@include file="navigation.jsp"%>
</header>
<section>
    <div class="container" style="margin-top:80px;">
            <div class="section-a">
                <div class="row">
                    <div class="col-sm-6">

                            <form action="/search" method="get">
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="Type Your Keyword Here">

                          <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Search</button>
                          </span>
                                </div><!-- /input-group -->
                                <input type="hidden" name="currentPage" value="1"/>
                            </form>


                    </div>
                </div>
            </div>
        <%
            ViewAndModel vm = (ViewAndModel)request.getAttribute("model");
            boolean resultExist = (vm != null && vm.getModel("result") != null);
        %>
        <c:if test="<%=resultExist%>">
            <div class="row">
                <hgroup class="mb20">
                    <h2 class="lead"><strong class="text-danger"><%=vm.getModel("totalResults")%></strong> results were found for <strong class="text-danger"><%=vm.getModel("keyword")%></strong></h2>
                </hgroup>

                <section class="col-sm-12">
                    <%
                        List<SearchResult> results = vm.getModel("result");
                        for(SearchResult sr : results){
                            out.println("  <article class=\"search-result row\">");
                            out.println("  <div class=\"col-md-2\">");
                            out.println("  <img style=\"width:140px;height:100px;\"  src=\"images/pdf-document.png\"/>");
                            out.println("</div>");
                            out.println("  <div class=\"col-md-9 excerpet\">");
                            out.println(String.format("<h3><a target='_blank' href='%s' title=''>%s</a></h3>", "/fileView?id=" + URLEncoder.encode(sr.getGcsLink(),"UTF-8"), sr.getTitle()));
                            out.println(String.format("<p>%s</p><span class='clearfix borda'/>",sr.getDescription()));
                            out.println(String.format("<h5 style='float:right'>%s<h5>",sr.getOwner()));
                            out.println("</article>");
                        }
                    %>

                    </section>
                <nav>
                    <ul class="pagination">

                        <%
                            int totalPages = vm.getModel("totalPages");
                            int currentPage = vm.getModel("currentPage");
                            String keyword = URLEncoder.encode(vm.getModel("keyword").toString(),"UTF-8");
                            for(int i = 1 ;  i <= totalPages; ++i){
                                out.println(String.format("<li><a href='/search?keyword=%s&currentPage=%d'>%d</a></li>",keyword,i,i));
                            }

                        %>



                    </ul>
                </nav>
            </div>
        </c:if>

        </div>



</section>

</body>
</html>
