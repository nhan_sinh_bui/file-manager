<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 9:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <%
        UserService userService = UserServiceFactory.getUserService();
        boolean loggedIn = userService.getCurrentUser() != null;
        String redirectUri = request.getParameter("redirectUri");
        if(redirectUri == null || redirectUri.trim().length() < 1){
            redirectUri="/profile.jsp";
        }
    %>


    <c:if test="<%=loggedIn%>">
        <h3>Welcome <%=userService.getCurrentUser().getEmail()%> [<a href="<%= userService.createLogoutURL(redirectUri)%>">Logout</a>]</h3>
    </c:if>
    <c:if test="<%=!loggedIn%>">
        <h3>Please login! [<a href="<%=userService.createLoginURL(redirectUri)%>">Google</a>]</h3>
    </c:if>

</body>
</html>
