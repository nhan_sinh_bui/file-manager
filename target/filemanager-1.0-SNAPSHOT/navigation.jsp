<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%--
  Created by IntelliJ IDEA.
  User: bui
  Date: 3/31/2015
  Time: 10:56 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" >Flystar</a>
        </div>
        <%
            String activeLink = (String)request.getAttribute("activeLink");
        %>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="<%=activeLink.equals("homepage")?"active":""%>"><a href="/home.jsp" >Home Page</a></li>
                <li class="<%=activeLink.equals("profile")?"active":""%>"><a href="/profile.jsp" >My Profile</a></li>
                <li class="<%=activeLink.equals("newFile")?"active":""%>"><a href="/uploadFile.jsp" >Add File</a></li>
                <li class="nav-divider"></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="<%=UserServiceFactory.getUserService().getCurrentUser() != null%>">
                    <li><a href=""><%=UserServiceFactory.getUserService().getCurrentUser().getEmail()%></a></li>
                    <li><a href="<%=UserServiceFactory.getUserService().createLogoutURL("/home.jsp")%>">Sign out</a></li>
                </c:if>
                <c:if test="<%=UserServiceFactory.getUserService().getCurrentUser() == null%>">

                    <li><a href="/auth.jsp">Sign in</a></li>
                </c:if>

            </ul>
        </div>
    </div>
</div>